﻿namespace System.Threading.Channels
{
    using System.Collections.Generic;




    /// <summary>
    /// 
    /// </summary>
    public class Channel<T> : IDisposable, IEnumerable<T>
    {
        #region Fields
        private readonly int _capacity;
        private bool _isClosed = false;
        private readonly Queue<T> _queue;
        private readonly object _syncGet = new object();
        private readonly object _syncSet = new object();
        #endregion




        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="capacity"></param>
        public Channel(int capacity = 1)
        {
            if (capacity < 1) throw new ArgumentOutOfRangeException("Capacity must be great than zero. Actual value of capacity is " + capacity);

            this._capacity = capacity;
            this._queue = new Queue<T>(capacity);
        }
        #endregion




        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public bool IsClosed
        {
            get { return this._isClosed; }
        }
        #endregion




        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            lock (this._syncGet)
                lock (this._syncSet)
                    if (!this._isClosed)
                    {
                        this._isClosed = true;
                        Monitor.PulseAll(this._syncGet);
                        Monitor.PulseAll(this._syncSet);
                    }
        }
        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            Close();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator()
        {
            return new ChannelEnumerator(this);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Collections.IEnumerator Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public T GetValue()
        {
            T value;

            if (!TryGetValue(out value))
                ThrowIfClosed();
            return value;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void SetValue(T value)
        {
            if (!TrySetValue(value))
                ThrowIfClosed();
        }
        /// <summary>
        /// 
        /// </summary>
        protected void ThrowIfClosed()
        {
            if (this._isClosed)
                throw new InvalidOperationException("Channel is closed");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="millisecondsTimeout"></param>
        /// <returns></returns>
        public virtual bool TryGetValue(out T value, int millisecondsTimeout = -1)
        {
            value = default(T);

            if (!this._isClosed)
            {
                Monitor.Enter(this._syncSet);
                Monitor.Enter(this._syncGet);
                bool getLock = true, setLock = true;
                try
                {
                    if (this._queue.Count == 0)
                    {
                        Monitor.Exit(this._syncSet);
                        setLock = false;
                        if (!Monitor.Wait(this._syncGet, millisecondsTimeout) || this._isClosed)
                            return false;
                    }

                    value = this._queue.Dequeue();
                    Monitor.Exit(this._syncGet); getLock = false;
                    lock (this._syncSet) Monitor.Pulse(this._syncSet);
                    return true;
                }
                finally
                {
                    if (getLock) Monitor.Exit(this._syncGet);
                    if (setLock) Monitor.Exit(this._syncSet);
                }
            }

            return false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="millisecondsTimeout"></param>
        /// <returns></returns>
        public virtual bool TrySetValue(T value, int millisecondsTimeout = -1)
        {
            if (!this._isClosed)
            {
                Monitor.Enter(this._syncSet);
                Monitor.Enter(this._syncGet);
                bool getLock = true, setLock = true;
                try
                {
                    if (this._queue.Count == this._capacity)
                    {
                        Monitor.Exit(this._syncGet);
                        getLock = false;
                        if (!Monitor.Wait(this._syncSet, millisecondsTimeout) || this._isClosed)
                            return false;
                    }

                    this._queue.Enqueue(value);
                    Monitor.Exit(this._syncSet); setLock = false;
                    lock (this._syncGet) Monitor.Pulse(this._syncGet);
                    return true;
                }
                finally
                {
                    if (getLock) Monitor.Exit(this._syncGet);
                    if (setLock) Monitor.Exit(this._syncSet);
                }
            }

            return false;
        }
        #endregion







        #region ChannelEnumerator<T>
        /// <summary>
        /// 
        /// </summary>
        public sealed class ChannelEnumerator : IEnumerator<T>
        {
            #region Fields
            private readonly Channel<T> _channel;
            private T _current;
            #endregion




            #region Constructors
            private ChannelEnumerator() { }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="channel"></param>
            public ChannelEnumerator(Channel<T> channel) { this._channel = channel; }
            #endregion




            #region Properties
            /// <summary>
            /// 
            /// </summary>
            public T Current
            {
                get { return this._current; }
            }
            /// <summary>
            /// 
            /// </summary>
            object Collections.IEnumerator.Current
            {
                get { return this.Current; }
            }
            #endregion




            #region Methods
            /// <summary>
            /// 
            /// </summary>
            public void Dispose()
            {
            }
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public bool MoveNext()
            {
                return this._channel.TryGetValue(out this._current);
            }
            /// <summary>
            /// 
            /// </summary>
            public void Reset()
            {
            }
            #endregion
        }
        #endregion
    }
}
